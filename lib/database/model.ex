defmodule Database.Model do
  use Ecto.Schema

  schema "coin" do
    field :name, :string
    field :price, :string
  end

  def changeset(coin, params \\ %{}) do
    coin
    |> Ecto.Changeset.cast(params, [:name, :price])
    |> Ecto.Changeset.validate_required([:name, :price])
  end

end
