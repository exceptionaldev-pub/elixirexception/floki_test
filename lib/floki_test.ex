defmodule FlokiTest do
  def main(args) do
    process()

  end

  def get_name(body) do
    body
    #        |> Floki.find(~s([href=\"/currencies/bitcoin/\"]]))
    |> Floki.find("[href=\"/currencies/bitcoin/\"]")
    |> Floki.text
  end

  def get_price(body) do
    body
    #    |> Floki.find(~s([data-btc="1.0"]))
    |> Floki.find("[data-btc=\"1.0\"]")
    |> Floki.attribute("data-usd")
    |> Floki.text
  end

  def process() do
    update()
    receive do
    after 5_000 ->
      update()
      process()
    end
  end

  def update() do
    url = "https://coinmarketcap.com/"
    #    body = HTTPoison.get!(url).body
    #    sc = HTTPoison.get!(url).status_code
    #    IO.puts(sc)
    #    IO.puts(getName(body) <> ": " <> getPrice(body))
    #    coin = %Database.Model{name: get_name(body), price: get_price(body)}

    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        coin = Database.Model
               |> Database.Repo.get_by(name: "BTCBitcoin")
        #           |> Database.Repo.get(1)
        changeset = Database.Model.changeset(coin, %{name: get_name(body), price: get_price(body)})
        Database.Repo.update(changeset)
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts "Not found :("
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect reason
    end


  end
end